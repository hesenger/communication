﻿using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Threading;

namespace Communication.Service
{
    public partial class Service1 : ServiceBase
    {
        private bool _running;
        private TcpListener _listener;

        public Service1()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            if (_listener == null)
            {
                _listener = new TcpListener(IPAddress.Any, 9090);
                _listener.Start();
            }

            _running = true;
            var t = new Thread(ProcessListener);
            t.IsBackground = true;
            t.Start();
        }

        protected override void OnStop()
        {
            _listener.Stop();
        }

        private void ProcessListener()
        {
            while (_running)
            {
                if (!_listener.Pending())
                    continue;

                var client = _listener.AcceptTcpClient();
                var t = new Thread(ProcessClient);
                t.IsBackground = true;
                t.Start(client);
            }
        }

        private void ProcessClient(object obj)
        {
            var client = (TcpClient)obj;
            while (_running)
            {
                if (!client.Connected)
                    return; // end connection when client is disconected

                if (client.Available == 0)
                    continue;

                var reader = new StreamReader(client.GetStream());
                var result = reader.ReadToEnd(); // expected string message over tcp in format: username,password
                var arr = result.Split(',');
                var username = arr[0];
                var password = arr[1];
                CreateUser(username, password);

                var writer = new StreamWriter(client.GetStream());
                writer.Write("OK");
            }
        }

        private void CreateUser(string username, string password)
        {
            // do your operation with database
        }
    }
}
