﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Communication.Gui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var client = new TcpClient();
            client.Connect("localhost", 9090);
            
            var writer = new StreamWriter(client.GetStream());
            writer.Write(string.Format("{0},{1}", textBox1.Text, textBox2.Text));
            while (true)
            {
                if (client.Available == 0)
                    continue;

                var reader = new StreamReader(client.GetStream());
                var result = reader.ReadToEnd();
                if (result == "OK")
                    break;

                MessageBox.Show("Create user failed:\n" + result);
            }

            client.Close();
            MessageBox.Show("User created!");
        }
    }
}
